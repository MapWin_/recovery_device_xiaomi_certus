ifneq ($(CERTUS_32_BUILD),true)
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
endif
$(call inherit-product, $(SRC_TARGET_DIR)/product/base.mk)
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from cactus device
$(call inherit-product, device/xiaomi/cactus/device.mk)

PRODUCT_MANUFACTURER := Xiaomi
PRODUCT_NAME := omni_cactus
PRODUCT_MODEL := Redmi 6A
PRODUCT_DEVICE := cactus
PRODUCT_BRAND := Xiaomi
